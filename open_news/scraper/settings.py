# # Scrapy settings for open_news project
# #
# # For simplicity, this file contains only the most important settings by
# # default. All the other settings are documented here:
# #
# #     http://doc.scrapy.org/topics/settings.html
# #

# import os, sys

# PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "example_project.settings")
# sys.path.insert(0, os.path.join(PROJECT_ROOT, "../../..")) #only for example_project


# BOT_NAME = 'open_news'

# SPIDER_MODULES = ['dynamic_scraper.spiders', 'open_news.scraper',]
# USER_AGENT = '%s/%s' % (BOT_NAME, '1.0')

# ITEM_PIPELINES = {
#     'dynamic_scraper.pipelines.DjangoImagesPipeline': 200,
#     'dynamic_scraper.pipelines.ValidationPipeline': 400,
#     'open_news.scraper.pipelines.DjangoWriterPipeline': 800,
# }

# IMAGES_STORE = os.path.join(PROJECT_ROOT, '../thumbnails')

# IMAGES_THUMBS = {
#     'medium': (50, 50),
#     'small': (25, 25),
# }

# DSCRAPER_IMAGES_STORE_FORMAT = 'ALL'

# DSCRAPER_LOG_ENABLED = True
# DSCRAPER_LOG_LEVEL = 'INFO'
# DSCRAPER_LOG_LIMIT = 5

import os
import sys
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

# sys.path.append('/home/social-net/mysite')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "socialnetwork.settings") #Changed in DDS v.0.3

BOT_NAME = 'open_news'

SPIDER_MODULES = ['dynamic_scraper.spiders','open_news.scraper']
USER_AGENT = '%s/%s' % (BOT_NAME, '1.0')

NEWSPIDER_MODULE = 'open_news.scraper'
# DEFAULT_ITEM_CLASS = 'open_news.items.BlogItem'
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'crwaler (+http://www.yourdomain.com)'
# DATABASE = {'drivername': 'sqlite3',
#             'host': '127.0.0.1',
#             'port': '8000',
#             'username': 'root',
#             'password': 'toor',
#             'database': 'db.sqlite3'}
ITEM_PIPELINES = {
    # 'dynamic_scraper.pipelines.DjangoImagesPipeline',
    'dynamic_scraper.pipelines.ValidationPipeline':400,
    'open_news.scraper.pipelines.DjangoWriterPipeline':800,
}

SPLASH_URL = 'http://192.168.59.103:8050'

DOWNLOADER_MIDDLEWARES = {
    'scrapyjs.SplashMiddleware': 725,
}

DUPEFILTER_CLASS = 'scrapyjs.SplashAwareDupeFilter'
HTTPCACHE_STORAGE = 'scrapyjs.SplashAwareFSCacheStorage'

# FEED_EXPORTERS = {
#         'sqlite': 'crwaler.exporters.SqliteItemExporter',
#     }
IMAGES_STORE = os.path.join(PROJECT_ROOT, '../thumbnails')

IMAGES_THUMBS = {
    'small': (170, 170),
}

DSCRAPER_LOG_ENABLED = True
DSCRAPER_LOG_LEVEL = 'INFO'
DSCRAPER_LOG_LIMIT = 5