from dynamic_scraper.spiders.django_spider import DjangoSpider
from open_news.models import NewsWebsite, Article, ArticleItem
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import HtmlResponse
from scrapy.contrib.loader import XPathItemLoader
from scrapy.contrib.loader.processor import TakeFirst, Join, MapCompose
from scrapy.spider import BaseSpider
from scrapy.http import Request
from scrapy.item import Item
from urlparse import urljoin
import re
import sys
import re
import json
from scrapy.spider import BaseSpider

from open_news.items import YelloBotItem, SggsBotItem, BlogItem, SBotItem

# from dynamic_scraper.spiders.django_spider import DjangoSpider
# from crwaler.models import NewsWebsite, Article, ArticleItem

class ArticleSpider(DjangoSpider):
    
    name = 'article_spider'

    def __init__(self, *args, **kwargs):
        self._set_ref_object(NewsWebsite, **kwargs)
        self.scraper = self.ref_object.scraper
        self.scrape_url = self.ref_object.url
        self.scheduler_runtime = self.ref_object.scraper_runtime
        self.scraped_obj_class = Article
        self.scraped_obj_item_class = ArticleItem
        super(ArticleSpider, self).__init__(self, *args, **kwargs)

class SggsBotCrawlSpider(CrawlSpider):

    name = 'sggsbotSpider'

    def __init__(self, *args, **kwargs):
        global rules
        # query = query.replace(' ','-')
        self.rules = (Rule(SgmlLinkExtractor(allow=('/'+'*')), callback='parse_item',follow = True),)
        super(SggsBotCrawlSpider, self).__init__(*args, **kwargs)
        self.allowed_domains = ['sggs.ac.in']
        self.start_urls = [kwargs.get('start_url')]
        print(self.start_urls)

    def parse_item(self,response):

        l = XPathItemLoader(item=SggsBotItem(),response=response)
        l.add_xpath('company','//*[@id="info-container"]/div[1]/h1/text()')
        l.add_xpath('street_address','//*[@id="info-container"]/div[1]/dl/dd[1]/span[1]/text()')
        l.add_xpath('locality','//*[@id="info-container"]/div[1]/dl/dd[1]/span[2]/text()')
        l.add_xpath('region','//*[@id="info-container"]/div[1]/dl/dd[1]/span[3]/text()')
        l.add_xpath('postalcode','//*[@id="info-container"]/div[1]/dl/dd[1]/span[4]/text()')


        res = l.load_item()

        results = {'name':'','address':''}

        if 'company' in res:
            results['name'] = res['company']
        if 'street_address' in res:
            results['address'] = res['street_address']
        if 'locality' in res:
            results['address'] = results['address'] + res['locality']
        if 'region' in res:
            results['address'] = results['address'] + res['region']
        if 'postalcode' in res:
            results['address'] = results['address'] + res['postalcode']

        return res

class BlogSpider(BaseSpider):

    name = 'blog'
    allowed_domains = ['www.greenhills.co.uk']
    start_urls = ['http://www.greenhills.co.uk/']
    seen = set()

    def parse(self,response):
        if response.url in self.seen:
            self.log('already seen %s ' % response.url)
        else:
            self.log('parsing %s' % response.url)
            self.seen.add(response.url)
        hxs = HtmlXPathSelector(response)
        if re.match(r'http://www.greenhills.co.uk/', response.url):
            item = BlogItem()
            item['title'] = hxs.select('//title/text()').extract()
            item['url'] = response.url
            item['text'] = hxs.select('//section[@id="main"]//child::node()/text()').extract()
            self.log("yielding item " + response.url)
            yield item
            # self.cursor.execute()

        for url in hxs.select('//a/@href').extract():
            url = urljoin(response.url, url)
            if not url in self.seen and not re.search(r'.(pdf|zip|jar)$', url):
                self.log("yielding request " + url)
                yield Request(url, callback=self.parse)

class SggsSpider(BaseSpider):
    name='sggsbot'
    allowed_domains = ['www.sggs.ac.in']
    start_urls = ['http://sggs.ac.in/sggs2012/']
    seen = set()

    def parse(self,response):
        if response.url in self.seen:
            self.log('already seen %s ' % response.url)
        else:
            self.log('parsing %s ' % response.url)
            self.seen.add(response.url)
        hxs = HtmlXPathSelector(response)
        if re.match(r'http://www.sggs.ac.in/', response.url):
            item = SBotItem()
            item['title'] = hxs.select('//title/text()').extract()
            item['url'] = response.url
            item['text'] = hxs.select('//section[@id="main"]//child::node()/text()').extract()
            self.log("yielding item" + response.url)
            yield item

        for url in hxs.select('//a/@href').extract():
            url = urljoin(response.url, url)
            if not url in self.seen and not re.search(r'.(pdf|zip|jar)$',url):
                self.log("yielding request "+ url)
                yield Request(url, callback=self.parse)
