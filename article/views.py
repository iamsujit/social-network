from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response
from article.models import Article

# Create your views here.
def articles(response):
	return render_to_response('articles.html',
		{ 'articles': Article.objects.all() })

def article(response, article_id=1):
	return render_to_response('article.html',
		{ 'article': Article.objects.get(id=article_id) })
