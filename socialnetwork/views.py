from django.shortcuts import render
from .forms import UsersForm


def about(request):
	return render(request, "about.html", {})

def profile(request):
	return render(request, "profile.html", {})

def form(request):
	title= 'Welcome'
	# if request.user.is_authenticated():
	# 	title = "My title %s" %(request.user)
	form = UsersForm(request.POST or None)
	context = {
		"title": title,
		"form":form
	}
	return render(request, "home.html", context)