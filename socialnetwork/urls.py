from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()
urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	# (r'^articles/', include('article.urls')),
	url(r'^$', include('search_engine.urls')),
	url(r'^college/', include('search_engine.urls')),
	url(r'^searchs/',include('search_engine.urls')),
	url(r'^university/', include('search_engine.urls')),
	url(r'^accounts/', include('registration.backends.default.urls')),
	url(r'^home/', 'socialnetwork.views.form', name='form'),

    # Examples
    url(r'^$', 'socialnetwork.views.about', name='about'),
    # url(r'^blog/', include('blog.urls')),
   
    
    url(r'^profile/', 'socialnetwork.views.profile', name='profile'),
    # url(r'^hello/$', 'article.views.hello'),
    # url(r'^login/$', 'article.views.template'),
)

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)