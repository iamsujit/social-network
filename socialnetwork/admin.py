from django.contrib import admin
from socialnetwork.models import Users
from .forms import UsersForm
# Register your models here.

class UsersAdmin(admin.ModelAdmin):
	list_display=('__unicode__','timestamp') 
	form = UsersForm
	# class Meta:
	# 	model = Users

admin.site.register(Users,UsersAdmin)