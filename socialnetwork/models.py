from django.db import models

class Users(models.Model):
	username=models.CharField(max_length=20)
	password=models.CharField(max_length=30)
	first_name=models.CharField(max_length=50)
	middle_name=models.CharField(max_length=20)
	last_name=models.CharField(max_length=50)
	email=models.CharField(max_length=50)
	branch=models.CharField(max_length=50)
	year=models.CharField(max_length=30)
	college=models.CharField(max_length=30)
	university=models.CharField(max_length=30)
	projects=models.TextField(blank=True)
	about_me=models.TextField(blank=True)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __unicode__(self):
		return self.username