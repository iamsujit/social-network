from django import forms
from .models import Users

class UsersForm(forms.ModelForm):
	class Meta:
		model = Users
		fields = ['username','password','first_name','middle_name','last_name','branch','year','college','university','projects','about_me']