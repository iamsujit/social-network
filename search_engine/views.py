from search_engine.models import Search,Crawler
from socialnetwork.models import Users
from django.shortcuts  import render_to_response,redirect
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.http import HttpResponse
from django.template.loader import get_template
from search_engine.search import normalize_query
from search_engine.search import get_query

def search(request):
    query_string = ''
    found_entries = None
    search_fields=('first_name','last_name','branch','year','projects','about_me','college')

    if ('q' in request.GET) and request.GET['q'].strip():

        query_string = request.GET['q']

        entry_query = get_query(query_string, search_fields)

        found_entries = Search.objects.filter(entry_query)
        found_entries = Users.objects.filter(entry_query)

    return render_to_response('search.html',
           { 'query_string': query_string, 'found_entries': found_entries},
           context_instance=RequestContext(request))

def searchs(response,search_id=1):
    return render_to_response('searchs.html',
        { 'searchs': Search.objects.get(id=search_id)})

def sggs(request):   
     query_string = ''
     query_string1 = 'sggs'
     found_entries = None
     search_fields=('url','text','title')

     

     if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, search_fields)
        entry_query1 = get_query(query_string1, search_fields)
        # found_entries = Search.objects.filter(entry_query1).filter(entry_query) 
        # found_entries = Users.objects.filter(entry_query1).filter(entry_query)  
        found_entries = Crawler.objects.filter(entry_query1).filter(entry_query)

     return render_to_response('search.html',
            {'query_string': query_string,'found_entries': found_entries},
            context_instance=RequestContext(request))

def mgm(request):   
     query_string = ''
     query_string1 = 'mgm'
     found_entries = None
     search_fields=('first_name','last_name','branch','year','projects','about_me','college')

     

     if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, search_fields)
        entry_query1 = get_query(query_string1, search_fields)
        found_entries = Search.objects.filter(entry_query1).filter(entry_query)  

     return render_to_response('search.html',
            {'found_entries': found_entries},
            context_instance=RequestContext(request))