from django.contrib import admin
from search_engine.models import Search,Crawler

# Register your models here.
class SearchAdmin(admin.ModelAdmin):
	list_display=('id','first_name','last_name','branch','year')
	search_fields=('first_name','last_name','branch','projects','about_me')

admin.site.register(Search,SearchAdmin)

class CrawlerAdmin(admin.ModelAdmin):
	list_display=('id','title')
	# search_fields=('first_name','last_name','branch','projects','about_me')

admin.site.register(Crawler,CrawlerAdmin)