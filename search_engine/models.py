from django.db import models

# Create your models here.
class Search(models.Model):
	username=models.CharField(max_length=20)
	first_name=models.CharField(max_length=50)
	middle_name=models.CharField(max_length=20)
	last_name=models.CharField(max_length=50)
	branch=models.CharField(max_length=50)
	year=models.CharField(max_length=30)
	college=models.CharField(max_length=30)
	university=models.CharField(max_length=30)
	projects=models.TextField(blank=True)
	about_me=models.TextField(blank=True)
	
class Crawler(models.Model):
	url = models.TextField(blank=True)
	text = models.TextField(blank=True)
	title = models.TextField(blank=True)